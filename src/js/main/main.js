$(document).ready(function () {

    //slinderin owl caruseli
    $('.owl__slider').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        mouseDrag: true,
        responsive: {

            320: {
                items: 1
            },
            600: {
                items: 2
            },
            800: {
                items: 3
            },
            1200: {
                items: 4,
                autoWidth:true
            }
        }





    })

    var selectormob = $('.owl__slider');

    //sag oxun elementin
    $('.my-next-button').click(function () {
        selectormob.trigger('next.owl.carousel');
    });

    //sol oxun elementin
    $('.my-prev-button').click(function () {
        selectormob.trigger('prev.owl.carousel');
    });


    //partnorsun owl caruseli
    $('.owl-partnors').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        responsive: {
            320: {
                items: 1
            },
            600: {
                items: 1
            },
            900: {
                items: 2
            },
            1000: {
                items: 3
            }
        }


    })

    var selector = $('.owl-partnors');

    //sag oxun elementin
    $('.my-next-button').click(function () {
        selector.trigger('next.owl.carousel');
    });

    //sol oxun elementin
    $('.my-prev-button').click(function () {
        selector.trigger('prev.owl.carousel');
    });




});



$('.header__sidebar--OpenIconbox').click(function () {

    $(".header__sidebar").toggleClass('active');

})

$('.header__sidebar--CloseIconbox').click(function () {

    $(".header__sidebar").toggleClass('active');

})









jQuery(document).ready(function ($) {
    var path = window.location.pathname.split('/').pop();

    if (path == '') {
        path = 'index.html';
    }

    var target = $('nav a[href="' + path + '"]');
    target.addClass('header__nav--active');
})